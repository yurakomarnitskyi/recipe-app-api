# REST API project
# Proxy

NGNX proxy fot our recipe app API

### Environment Variables

* `LISTEN_PORT` - Port to listen (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

## Opportunities 

The project presents an API where Token authorization is implemented, the user can create a recipe with ingredients, tags, and you can also upload photos to them. This REST API uses CRUD operations.

### Technology

Python, Django, Django Rest Framework, PostgreSQL, Test Drive Development, AWS, Docker, Nginx

### Use the following commands to run docker-compose on your computer or if you want run AWS server open deployment.md file:

```sh
docker-compose up
```

### Run test and flake8
```sh
docker-compose run --rm app sh -c "python manage.py test && flake8"
```